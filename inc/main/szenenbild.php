<section id="szenenbild">	
	<div id="work-container" class="row">
		<?php
			$sql = mysqli_query($db, "SELECT * FROM szenenbild ORDER BY id DESC LIMIT 0, 6");
			while($row = mysqli_fetch_assoc($sql)){
				echo "<div class='large-4 columns'>";
					echo "<div id='".$row['id']."' class='work'>";
						echo "<img src='img/upload_images/".$row['img']."' alt='".$row['title']."' />";
						echo "<div class='work-info'>";
							echo "<h6>".$row['title']."</h6>";
						echo "</div>";
					echo "</div>";
				echo "</div>";
			}
		?>
	</div>
</section>
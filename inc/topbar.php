<div id="top-bar">
	<div class="row">
		<div class="large-4 columns">
			<div id="logo-lettering" >
				<a class="scroll" href="#header">vicky von minckwitz</a>
			</div>
			<div id="menu-mobile" class="show-for-small">
			</div>
		</div>
		<div class="large-8 columns">
			<nav id="top-navigation">
				<ul class="inline-list">
					<li><a href="#szenenbild">Szenenbild</a></li>
					<li><a href="#art_direction">Art Direktion</a></li>
					<li><a href="#sign_design">Sign Design</a></li>
					<li><a href="#archive">Archiv</a></li>
					<li><a href="#contact">Kontakt</a></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
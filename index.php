<?php require("db.php"); ?>
<!DOCTYPE html>
<html class="no-js" lang="de">

<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	
	<title>Vicky von Minckwitz</title>

	<link href="favicon1.ico" rel="icon" type="image/x-icon" />
	
	<link rel="stylesheet" href="css/normalize.css" />
	<link rel="stylesheet" href="css/foundation.css" />
	<link rel="stylesheet" href="css/styles.css" />
	<link rel="stylesheet" href="css/custom.css" />

	
	<link href='http://fonts.googleapis.com/css?family=Cabin:400,700,400italic|Merriweather:900' rel='stylesheet' type='text/css'>
	<script src="js/vendor/custom.modernizr.js"></script>

</head>
<body>
	
	<!-- START PAGE -->
	<div id="page">	
	
		<!-- START HEADER -->
		<header id="header" class="smooth">
			<div class="row">
				<div class="large-12 columns">
					<div id="intro">
						<div id="logo">
							&nbsp;
						</div>
						<div id="explore"><a class="scroll" href="#szenenbild"><img src="img/scroll.svg" alt="Scroll"/></a></div>
					</div>
				</div>
			</div>
		</header>
		<!-- END HEADER -->
		
		<!-- START TOP-BAR -->
		<?php require("inc/topbar.php"); ?>
		<!-- END TOP-BAR -->	
		
		<!-- START SZENENBILD SECTION -->
		<?php require("inc/main/szenenbild.php"); ?>
		<!-- END SZENENBILD SECTION -->
		
		<!-- START ART DIREKTION SECTION -->
		<section id="art_direction">
			<div id="work-container" class="row">
				<div class="large-4 columns">
					<div id="work-1" class="work">
						<img src="img/upload_images/pic1.jpg" alt="work" />
						<div class="work-info">
							<h6>Sample Caption 1</h6>
						</div>
					</div>
				</div>
				<div class="large-4 columns">
					<div id="work-2" class="work">
						<img src="img/upload_images/pic2.jpg" alt="work"  />
						<div class="work-info">
							<h6>Sample Caption 2</h6>
						</div>
					</div>
				</div>
				<div class="large-4 columns end">
					<div id="work-3" class="work">
						<img src="img/upload_images/pic3.jpg" alt="work" />
						<div class="work-info">
							<h6>Sample Caption 3</h6>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END ART DIREKTION SECTION -->

		<!-- START SIGNDESIGN SECTION -->
		<section id="sign_design">
			<div id="work-container" class="row">
				<div class="large-4 columns">
					<div id="work-1" class="work">
						<img src="img/upload_images/pic1.jpg" alt="work" />
						<div class="work-info">
							<h6>Sample Caption 1</h6>
						</div>
					</div>
				</div>
				<div class="large-4 columns">
					<div id="work-2" class="work">
						<img src="img/upload_images/pic2.jpg" alt="work"  />
						<div class="work-info">
							<h6>Sample Caption 2</h6>
						</div>
					</div>
				</div>
				<div class="large-4 columns end">
					<div id="work-3" class="work">
						<img src="img/upload_images/pic3.jpg" alt="work" />
						<div class="work-info">
							<h6>Sample Caption 3</h6>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END SIGNDESIGN SECTION -->

		<!-- START ARCHIV SECTION -->
		<section id="archive">
			<div class="studio">
				<div class="row">
					<div class="large-12 columns">
						<div id="swiper" class="swiper-container">
							<div class="swiper-wrapper">
								<div class="swiper-slide"><img src="img/upload_images/pic1.jpg" alt="slider image 1" ></div>
								<div class="swiper-slide"><img src="img/upload_images/pic2.jpg" alt="slider image 2"></div>
								<div class="swiper-slide"><img src="img/upload_images/pic3.jpg" alt="slider image 3"/></div>
								<div class="swiper-slide"><img src="img/upload_images/pic4.jpg" alt="slider image 4"/></div>
								<div class="swiper-slide"><img src="img/upload_images/pic5.jpg" alt="slider image 5"/></div>
								<div class="swiper-slide"><img src="img/upload_images/pic6.jpg" alt="slider image 6"/></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- END ARCHIV SECTION -->

		<!-- START CONTACTS SECTION -->
		<section id="contacts">
			<div class="row">
				<div class="large-6 large-offset-3 columns">
					<div class="contact-info">
						<div class="row">
							<div class="large-4 columns contact">
								<div class="icon">
									<img class="mail" src="img/icons/mail.svg" alt="mail"/>
								</div>
								<p>
									<a href="mailto:hallo@vickyvonminckwitz.de">hallo@vickyvonminckwitz.de</a>
								</p>
							</div>
							<div class="large-4 columns contact">
								<div class="icon">
									<img src="img/icons/location.svg" alt="location"/>
								</div>
								<p>
									Atelier Raumproduktion<br/>
									<a href="https://www.google.de/maps/preview/place/Seilerstra%C3%9Fe+33/@53.5503404,9.9643274,17z/data=!3m1!4b1!4m2!3m1!1s0x47b18f6d8dd65a8b:0xe27db507780f7356" target="_blank">Seilerstrasse 33</a><br/>
									20359 Hamburg / Germany
								</p>
							</div>
							<div class="large-4 columns contact">
								<div class="icon">
									<img src="img/icons/contact.svg" alt="contact"/>
								</div>
								<p>
									T <a href="tel:+494028466574">+49 (0)40 / 28 46 65 74</a><br/>
									F +49 (0)40 / 28 46 65 71<br/>
									M <a href="tel:+491632042776">+49 (0)163 / 204 27 76</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		
		<!-- START FOOTER SECTION -->
		<section id="footer">
			<div class="row">
				<div class="large-8 large-offset-2 columns">
					2015 Vicky von Minckwitz &ndash; Impressum
				</div>
			</div>
		</section>
		<!-- END FOOTER SECTION -->

	</div>
	<!-- END PAGE -->
	
	<!-- START PROJECT PAGE -->
	<div id="project-page">
	
		<!-- START PROJECT-TOP-BAR -->
		<div id="project-top-bar">
			<div class="row">
				<div class="large-5 small-2 columns">
					<div id="previous-project"></div>
					<div id="previous-project-name"></div>
				</div>
				<div class="large-2 small-8 columns">
					<div id="close-project"></div>
				</div>
				<div class="large-5 small-2 columns">
					<div id="next-project"></div>
					<div id="next-project-name"></div>
				</div>
			</div>
		</div>	
		<!-- END PROJECT-TOP-BAR -->
		
		<!-- START PROJECT -->
		<div id="project"></div>
		<!-- END PROJECT -->
	</div>
	<!-- END PROJECT PAGE -->
	
	<!-- Foundation Scripts -->
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
		
	<script>$(document).foundation();</script>

	<!-- Jquery Scripts -->
	<script src="js/jquery.mixitup.min.js"></script>
	<script src="js/jquery.easing.js"></script>
	
	<!-- Swiper Plugin -->
	<script  src="js/idangerous.swiper-1.9.3.js"></script>
	
	<!-- Feed Plugins -->
	<script src="js/jquery.tweet.js"></script>
	<script src="js/jquery.flickr.js"></script>
	<script src="js/jquery.instagram.js"></script>
	
	<!-- Cotton Specific Scripts -->
	<script src="js/scripts.js"></script>
	
</body>
</html>
